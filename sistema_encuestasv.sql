-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: sistema_encuestasv
-- ------------------------------------------------------
-- Server version	5.7.39-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `encuestas`
--

DROP TABLE IF EXISTS `encuestas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `encuestas` (
  `id_encuesta` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fecha_final` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_encuesta`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuestas`
--

LOCK TABLES `encuestas` WRITE;
/*!40000 ALTER TABLE `encuestas` DISABLE KEYS */;
INSERT INTO `encuestas` VALUES (6,'1234','Auditoria','Cuestionario de control interno para la seguridad en aspectos técnicos de la aplicación.',1,'2022-11-07 22:07:07','2022-11-18 22:07:07');
/*!40000 ALTER TABLE `encuestas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opciones`
--

DROP TABLE IF EXISTS `opciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opciones` (
  `id_opcion` int(11) NOT NULL AUTO_INCREMENT,
  `id_pregunta` int(11) NOT NULL,
  `valor` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_opcion`),
  KEY `id_pregunta` (`id_pregunta`),
  CONSTRAINT `opciones_ibfk_1` FOREIGN KEY (`id_pregunta`) REFERENCES `preguntas` (`id_pregunta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opciones`
--

LOCK TABLES `opciones` WRITE;
/*!40000 ALTER TABLE `opciones` DISABLE KEYS */;
INSERT INTO `opciones` VALUES (14,7,'Muy de acuerdo'),(15,7,'Algo de acuerdo'),(16,7,'Ni acuerdo ni en desacuerdo'),(17,7,'Algo desacuerdo'),(18,7,'Muy en desacuerdo'),(19,8,'Muy de acuerdo'),(20,8,'Algo de acuerdo'),(21,8,'Ni acuerdo ni en desacuerdo'),(22,8,'Algo desacuerdo'),(23,8,'Muy en desacuerdo'),(24,9,'Muy de acuerdo'),(25,9,'Algo de acuerdo'),(26,9,'Ni acuerdo ni en desacuerdo'),(27,9,'Algo desacuerdo'),(28,9,'Muy en desacuerdo'),(29,10,'Muy de acuerdo'),(30,10,'Algo de acuerdo'),(31,10,'Ni acuerdo ni en desacuerdo'),(32,10,'Algo desacuerdo'),(33,10,'Muy en desacuerdo'),(34,11,'Muy de acuerdo'),(35,11,'Algo de acuerdo'),(36,11,'Ni acuerdo ni en desacuerdo'),(37,11,'Algo desacuerdo'),(38,11,'Muy en desacuerdo'),(39,12,'Muy de acuerdo'),(40,12,'Algo de acuerdo'),(41,12,'Ni acuerdo ni en desacuerdo'),(42,12,'Algo desacuerdo'),(43,12,'Muy en desacuerdo'),(44,13,'Muy de acuerdo'),(45,13,'Algo de acuerdo'),(46,13,'Ni acuerdo ni en desacuerdo'),(47,13,'Algo desacuerdo'),(48,13,'Muy en desacuerdo'),(49,14,'Muy de acuerdo'),(50,14,'Algo de acuerdo'),(51,14,'Ni acuerdo ni en desacuerdo'),(52,14,'Algo desacuerdo'),(53,14,'Muy en desacuerdo'),(54,15,'Muy de acuerdo'),(55,15,'Algo de acuerdo'),(56,15,'Ni acuerdo ni en desacuerdo'),(57,15,'Algo desacuerdo'),(58,15,'Muy en desacuerdo'),(59,16,'Muy de acuerdo'),(60,16,'Algo de acuerdo'),(61,16,'Ni acuerdo ni en desacuerdo'),(62,16,'Algo desacuerdo'),(63,16,'Muy en desacuerdo'),(66,17,'Muy de acuerdo'),(67,17,'Algo de acuerdo'),(68,17,'Ni acuerdo ni en desacuerdo'),(69,17,'Algo desacuerdo'),(70,17,'Muy en desacuerdo'),(71,18,'Muy de acuerdo'),(72,18,'Algo de acuerdo'),(73,18,'Ni acuerdo ni en desacuerdo'),(74,18,'Algo desacuerdo'),(75,18,'Muy en desacuerdo'),(76,19,'Muy de acuerdo'),(77,19,'Algo de acuerdo'),(78,19,'Ni acuerdo ni en desacuerdo'),(79,19,'Algo desacuerdo'),(80,19,'Muy en desacuerdo'),(81,20,'Muy de acuerdo'),(82,20,'Algo de acuerdo'),(83,20,'Ni acuerdo ni en desacuerdo'),(84,20,'Algo desacuerdo'),(85,20,'Muy en desacuerdo'),(91,21,'Muy de acuerdo'),(92,21,'Algo de acuerdo'),(93,21,'Ni acuerdo ni en desacuerdo'),(94,21,'Algo desacuerdo'),(95,21,'Muy en desacuerdo');
/*!40000 ALTER TABLE `opciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preguntas`
--

DROP TABLE IF EXISTS `preguntas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `preguntas` (
  `id_pregunta` int(11) NOT NULL AUTO_INCREMENT,
  `id_encuesta` int(11) NOT NULL,
  `titulo` varchar(900) COLLATE utf8_unicode_ci NOT NULL,
  `id_tipo_pregunta` int(11) NOT NULL,
  PRIMARY KEY (`id_pregunta`),
  KEY `id_encuesta` (`id_encuesta`),
  KEY `id_tipo_pregunta` (`id_tipo_pregunta`),
  CONSTRAINT `preguntas_ibfk_1` FOREIGN KEY (`id_tipo_pregunta`) REFERENCES `tipo_pregunta` (`id_tipo_pregunta`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `preguntas_ibfk_2` FOREIGN KEY (`id_encuesta`) REFERENCES `encuestas` (`id_encuesta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preguntas`
--

LOCK TABLES `preguntas` WRITE;
/*!40000 ALTER TABLE `preguntas` DISABLE KEYS */;
INSERT INTO `preguntas` VALUES (7,6,'¿Existe control sobre claves de acceso al sistema? ',1),(8,6,'¿Tiene la aplicación manual técnico, de operación y del usuario? ',1),(9,6,'¿Con qué periodicidad se le saca back-up (copia de seguridad) a la aplicación? ',1),(10,6,'¿Pueden los operadores o usuarios modificar los programas fuente de la aplicación? ',1),(11,6,'¿Se permite el uso de la computadora a personas extrañas a la dependencia? ',1),(12,6,'¿Existe más de un funcionario que esté en capacidad de operar la aplicación? ',1),(13,6,'¿Hay información de carácter confidencial o privada relacionada con la aplicación? ',1),(14,6,'¿Existen procedimientos formales que contemplen la seguridad física y lógica de los datos transmitidos entre redes, de tal manera que garanticen la op',1),(15,6,'¿Existe pérdida de mensajes en el sistema de conmutación de datos? ¿Con qué frecuencia se presenta? ',1),(16,6,'¿Brinda el sistema de administración de la base de datos una adecuada protección a la información y datos almacenados? ',1),(17,6,'¿Se revisa la bitácora de la base de datos? ',1),(18,6,'¿Existen estándares relacionados con los programas de la aplicación? ',1),(19,6,'¿Existe documentación escrita relacionada con el respaldo y recuperación de la base de datos en caso de presentarse destrucción parcial o total de ést',1),(20,6,'¿Es empleada la criptografía para proteger el almacenamiento y transmisión de información? ',1),(21,6,'¿Es tenido en cuenta el criterio de los usuarios de la aplicación durante las diferentes etapas del ciclo de vida del sistema? ',1);
/*!40000 ALTER TABLE `preguntas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resultados`
--

DROP TABLE IF EXISTS `resultados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resultados` (
  `id_resultado` int(11) NOT NULL AUTO_INCREMENT,
  `id_opcion` int(11) NOT NULL,
  PRIMARY KEY (`id_resultado`),
  KEY `id_opcion` (`id_opcion`),
  CONSTRAINT `resultados_ibfk_1` FOREIGN KEY (`id_opcion`) REFERENCES `opciones` (`id_opcion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resultados`
--

LOCK TABLES `resultados` WRITE;
/*!40000 ALTER TABLE `resultados` DISABLE KEYS */;
INSERT INTO `resultados` VALUES (17,14),(18,19),(19,24),(20,29),(21,34),(22,39),(23,44),(24,49),(25,54),(26,59),(27,66),(28,71),(29,76),(30,81),(31,91);
/*!40000 ALTER TABLE `resultados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuarios`
--

DROP TABLE IF EXISTS `tb_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_usuarios` (
  `usuario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(130) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `celular` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ultima_sesion` datetime DEFAULT NULL,
  `activacion` int(1) NOT NULL DEFAULT '0',
  `token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `token_password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_request` int(11) DEFAULT '0',
  `fecha_registro` datetime NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`usuario`),
  KEY `usuario` (`usuario`),
  KEY `usuarios_ibfk_1` (`id_tipo_usuario`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tipo_usuario` (`id_tipo_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuarios`
--

LOCK TABLES `tb_usuarios` WRITE;
/*!40000 ALTER TABLE `tb_usuarios` DISABLE KEYS */;
INSERT INTO `tb_usuarios` VALUES ('xiomara','$2y$10$sRKv.NZAKRvQXYcq4mzcq.f/.epZ6nnSUtpDQ9Xseg/e0a9DSVeyW','fernanda','3828344703','xjimenez@ucundinamarca.edu.co','2022-11-15 19:45:02',1,'1b3b0d310a13380fc9a01af27d31cdfe','',1,'2022-11-15 09:24:45',1),('xiomara1509','$2y$10$mdUFySNsHTecE5gPi2ZyOOBPhu3oAIIkziFCUZYJo33RTYEa5bSSq','Xiomara aya','3024673833','xiomara18092001@gmail.com','2022-11-15 19:44:46',1,'271ea259f2e84bd29a864a4dd53281f3','',1,'2022-11-15 09:24:13',2);
/*!40000 ALTER TABLE `tb_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_pregunta`
--

DROP TABLE IF EXISTS `tipo_pregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_pregunta` (
  `id_tipo_pregunta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_tipo_pregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_pregunta`
--

LOCK TABLES `tipo_pregunta` WRITE;
/*!40000 ALTER TABLE `tipo_pregunta` DISABLE KEYS */;
INSERT INTO `tipo_pregunta` VALUES (1,'Selección múltiple','Se podrá escoger solo una opción\r\nelemento input type radio'),(2,'Desplegable','Se podrá escoger una opción\r\nElemento select y option'),(3,'Casilla de verificación','Se podrá escoger más de una opción\r\ninput type checkbox'),(4,'Texto','Se almacenara la respuesta');
/*!40000 ALTER TABLE `tipo_pregunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,'Administrador'),(2,'Usuario');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_encuestas`
--

DROP TABLE IF EXISTS `usuarios_encuestas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_encuestas` (
  `id_usuario` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `id_encuesta` int(11) NOT NULL,
  KEY `usuario` (`id_usuario`),
  KEY `id_encuesta` (`id_encuesta`),
  CONSTRAINT `usuarios_encuestas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`usuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarios_encuestas_ibfk_2` FOREIGN KEY (`id_encuesta`) REFERENCES `encuestas` (`id_encuesta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_encuestas`
--

LOCK TABLES `usuarios_encuestas` WRITE;
/*!40000 ALTER TABLE `usuarios_encuestas` DISABLE KEYS */;
INSERT INTO `usuarios_encuestas` VALUES ('xiomara1509',6);
/*!40000 ALTER TABLE `usuarios_encuestas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-15 20:01:51
