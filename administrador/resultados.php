<?php 

	include '../config.php';
	$id_encuesta = $_GET['id_encuesta'];

	/* Consulta para extraer título y descripción de la encuesta*/
	$query3 = "SELECT * FROM encuestas WHERE id_encuesta = '$id_encuesta'";
	$resultados3 = $conexion->query($query3);
	$row3 = $resultados3->fetch_assoc();

 ?>

<!DOCTYPE html>
<html lang="es">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <!-- Favicon - FIS -->
  <link rel="shortcut icon" href="../imagenes/Logo-fis.png">


  <title>Resultados</title>
  <link rel="icon" type="image/png" href="https://cdn-icons-png.flaticon.com/512/3833/3833453.png">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="javascript:void(0)">Sistema de Encuestas</a>
     
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
        <span class="navbar-toggler-icon"></span>
      </button>
    

      <!--NAVBAR-->
      <div class="collapse navbar-collapse" id="navb">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0" style="color: #fff">
          
		  	<?php   
	      	 session_start();
			   $id_usuario = $_SESSION['usuario'];
			   include "../config.php";
			   $query = "SELECT * FROM tb_usuarios WHERE usuario = '$id_usuario'";
			   $resultado = $conexion->query($query);
			   if ($row2 = $resultado->fetch_assoc())
				{
				 if ($row2['id_tipo_usuario'] == '1') {
				  echo "Bienvenido ". $_SESSION['usuario'] . "\t";;
				  echo "<a href='../cerrar_sesion.php' class='btn btn-danger' style='margin-left: 10px'>Cerrar Sesión</a>";
				} else{
					header("Location: ../usuario/index.php");
				  }
			  }else{
			  header("location: ../index.php");
			  }
			 include "../inc/footer.php";
	       ?>
        </form>
      </div>
  	</nav>

  	<div class="container" style="margin-top: 50px;">
  		
  	<?php

  	$consulta = "SELECT * FROM preguntas WHERE id_encuesta = '$id_encuesta'";
	$resultados2 = $conexion->query($consulta);

	 ?>

	<hr/>
	<div class="container text-center">
		<h1><?php echo $row3['titulo'] ?></h1>
		<p><?php echo $row3['descripcion'] ?></p>
	</div>
	<hr/>

	<?php
		$suma=0;
		$cont = 0;
		$cont2=0;
		$consulta = "SELECT * FROM usuarios_encuestas";
	    $resultados3 = $conexion->query($consulta);
		if($row3 = $resultados3->fetch_assoc()==true){
		while ($row2 = $resultados2->fetch_assoc()) {
		
		$id_pregunta = $row2['id_pregunta'];

		$query = "SELECT preguntas.id_pregunta, preguntas.titulo,COUNT('preguntas.titulo') as count, opciones.valor FROM opciones INNER JOIN preguntas ON opciones.id_pregunta=preguntas.id_pregunta INNER JOIN resultados ON opciones.id_opcion=resultados.id_opcion WHERE preguntas.id_pregunta = '$id_pregunta' GROUP BY opciones.valor ORDER BY preguntas.id_pregunta";
		$resultados = $conexion->query($query);

				/*TITULO*/
		$cont++;
		echo "<h3>" .$cont.". ". $row2['titulo'] . "</h3>";

		$cantidades = array();
		$titulos = array();
		$tamaño = array(); 
		$i = 1;
		while ($row = $resultados->fetch_assoc()) {
			$cont2++;
			$resp = $row['valor'];
			echo "<h3><b>Respuesta: </b>". $resp . "</h3>";
			if($resp=="Muy de acuerdo"){
				$suma += 5;
			}
			if($resp=="Algo de acuerdo"){
				$suma += 4;
			}
			if($resp=="Ni acuerdo ni en desacuerdo"){
				$suma += 3;
			}
			if($resp=="Algo desacuerdo"){
				$suma += 2;
			}
			if($resp=="Muy en desacuerdo"){
				$suma += 1;
			}
		}

		$opciones = $i - 1;
		for ($i = 1; $i <= $opciones; $i++) {
		?>

		<input type="hidden" class="<?php echo "valor$i" ?>" value="<?php echo $cantidades[$i] ?>">
		<input type="hidden" class="<?php echo "titulo$i" ?>" value="<?php echo $titulos[$i] ?>">

		<?php  
		}/*95*/
		 ?>

		<input type="hidden" class="tamaño" value="<?php echo $opciones ?>">
		<br>
		<hr/>

		<script src="js/resultados.js">

		</script>

	<?php


	}
	
  	 ?>
	<div class="container text-center" style="margin-bottom: 20px">
	<?php
		   $prom = $suma/$cont2; ?>

			
		<a href="reporte.php?promedio=<?php echo $prom; ?>" class="btn btn-primary">GENERAR REPORTE</a>
	<?php
	    }else{
			echo "No hay respuestas";
		}
	 ?>	
	</div>
</body>
</html>