

<!DOCTYPE html>
<html lang="es">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <!-- Favicon - FIS -->
  <link rel="shortcut icon" href="../imagenes/Logo-fis.png">


  <title>Sistema de encuestas</title>
  <link rel="icon" type="image/png" href="https://cdn-icons-png.flaticon.com/512/3833/3833453.png">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
</head>
<body>


	
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="javascript:void(0)">Sistema de Encuestas</a>
     
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
        <span class="navbar-toggler-icon"></span>
      </button>
    

      <!--NAVBAR-->
      <div class="collapse navbar-collapse" id="navb">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0" style="color: #fff">
          
		  	<?php   
	      	 session_start();
			   $id_usuario = $_SESSION['usuario'];
			   include "../config.php";
			   $query = "SELECT * FROM tb_usuarios WHERE usuario = '$id_usuario'";
			   $resultado = $conexion->query($query);
			   if ($row2 = $resultado->fetch_assoc())
				{
					if ($row2['id_tipo_usuario'] == '1') {
				  echo "Bienvenido ". $_SESSION['usuario'] . "\t";;
				  echo "<a href='../cerrar_sesion.php' class='btn btn-danger' style='margin-left: 10px'>Cerrar Sesión</a>";
				} else{
					header("Location: ../usuario/index.php");
				  }
			  }else{
			  header("location: ../index.php");
			  }
			 include "../inc/footer.php";
	       ?>




        </form>
      </div>
  	</nav>
  	
  	<center>
 	<div class="container text-center">
	 <hr /> 
	 <div class="form-group text-center">
	                    	
	                    	<h1><strong>Reporte</strong></h1>
								 
			</div>
		


		
 		<form action="procesar.php" method="Post" autocomplete="off">
 		<input type="hidden" id="id_encuesta" name="id_encuesta" value="<?php echo $id_encuesta ?>" />

 		<hr />
		 
		 <?php


$prom= $_GET['promedio'];

if ($prom <= 5 && $prom >= 4) {
	echo $comp ='<img src="verde.png" "/>';


	}else if ($prom < 4 && $prom >= 3) {
		
		echo $comp ='<img src="amarillo.png" "/>';
		
			}else if ($prom < 3 && $prom >= 2) {
				echo $comp ='<img src="naranja.png" "/>';
			

				}else  {
			
				echo $comp ='<img src="rojo.png" "/>';
				}
?>
	</center>
<div class="form-group text-center">
	 <p>De acuerdo con los resultados obtenidos nuestro sistema sugiere: </p>
</div>


<div class="container text">
<?php

	$prom= $_GET['promedio'];

if ($prom <= 4 && $prom >= 1) {
	echo "<hr>";
	echo "<b>Aspectos por mejorar:</b> ";
	echo "<br>";
	echo "<hr>";
	
	echo "<b>¿Existe control sobre claves de acceso al sistema?</b> ";
	echo "<br>";
	echo "Se debe inhabilitar la clave de acceso de aquellos usuarios que entran a disfrutar de vacaciones o son retirados de la empresa.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Tiene la aplicación manual técnico, de operación y del usuario?</b> ";
	echo "<br>";
	echo "Toda aplicación debe tener los manuales técnicos, de operación y del usuario actualizados y preferiblemente disponibles en línea, con el propósito de consultarlos cuando se requiera.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Con qué periodicidad se le saca back-up (copia de seguridad) a la aplicación?</b> ";
	echo "<br>";
	echo "Se debe diseñar un procedimiento que consigne todo lo relativo a las copias de seguridad, como son los instructivos para llevarlas a cabo, periodicidad, persona responsable de su ejecución, número de copias a realizar, lugar de almacenamiento de las copias, etc.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Pueden los operadores o usuarios modificar los programas fuente de la aplicación?</b> ";
	echo "<br>";
	echo "Se deben diseñar las salvaguardas necesarias para que los programas de la aplicación no sean modificados, sustituidos o ejecutados por personas no autorizadas.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Se permite el uso de la computadora a personas extrañas a la dependencia?</b> ";
	echo "<br>";
	echo "Se debe prohibir el acceso a personas ajenas al sistema de computación y como medida precautelativa se debe exigir que el ingreso a la aplicación siempre se haga a través de menús obligatorios y con solicitud de clave de acceso.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Existe más de un funcionario que esté en capacidad de operar la aplicación?</b> ";
	echo "<br>";
	echo "Se debe establecer como política que la capacitación debe ser impartida a más de un funcionario por aplicación, con el fin de prever tropiezos en caso de presentarse enfermedad o abandono del cargo por parte del funcionario que está al frente de la operación de la aplicación.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Hay información de carácter confidencial o privada relacionada con la aplicación?</b> ";
	echo "<br>";
	echo "En caso de tenerse que ver con información de carácter confidencial, se debe poner especial cuidado en su manejo y almacenamiento, procurando garantizar la integridad de la información.";
	echo "<br>";
	echo "<hr>";
	
	echo "<b>¿Existen procedimientos formales que contemplen la seguridad física y lógica de los datos transmitidos entre redes, de tal manera que garanticen la oportunidad totalidad y exactitud de estos?</b>";
	echo "<br>";
	echo "Se deben implementar los controles necesarios que garanticen la protección de los datos que son transmitidos por la red, impidiendo el monitoreo no autorizado, controlando y resguardando los accesos físicos y lógicos a la red, resguardando el software de comunicaciones, así como su respectiva documentación.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Existe pérdida de mensajes en el sistema de conmutación de datos? ¿Con qué frecuencia se presenta?</b> ";
	echo "<br>";
	echo "Se debe controlar la pérdida o modificación de los mensajes durante la transmisión mediante su almacenamiento y posterior reenvió, (sistema de retardo).";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Brinda el sistema de administración de la base de datos una adecuada protección a la información y datos almacenados?</b> ";
	echo "<br>";
	echo "Los sistemas de gestión de bases de datos (DBMS) ofrecen a desarrolladores, administradores y usuarios, una gama muy amplia de herramientas que permiten garantizar la integridad, consistencia, confidencialidad, confiabilidad y en general la seguridad de la información almacenada y con un elemento muy importante a favor: las líneas de código que se requieren por parte del implementador son mínimas, en ocasiones solo basta con una sencilla sentencia para obligar al DBMS a controlar y mantener las restricciones necesarias.";
	echo "<br>";
	echo "<hr>";
	
	echo "<b>¿Se revisa la bitácora de la base de datos? </b>";
	echo "<br>";
	echo "Se debe revisar regularmente la bitácora de reinicios y reprocesos causados por el mal funcionamiento del sistema y analizar las causas que hayan podido generar tal dificultad para que se puedan corregir apropiadamente.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Existen estándares relacionados con los programas de la aplicación? </b>";
	echo "<br>";
	echo "Debe haber estándares escritos que orienten al diseño de los programas de la aplicación en donde se especifiquen las llamadas de datos que puedan ser usadas para acceder a la base de datos. La documentación debe contener las entidades, atributos e interrelaciones y debe ser revisada por el administrador de la base de datos antes de que el programa sea puesto en producción.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Existe documentación escrita relacionada con el respaldo y recuperación de la base de datos en caso de presentarse destrucción parcial o total de ésta?</b> ";
	echo "<br>";
	echo "Se debe verificar que haya una documentación específica para el respaldo y recuperación de la base de datos; que considere el copiado de la base de datos a intervalos regulares permitiendo su recuperación automática. La documentación debe incluir procedimientos y estándares para el reinicio y/o recuperación de los datos. El administrador de la base de datos es el responsable de garantizar la integridad, confidencialidad y confiabilidad de la base de datos e igualmente debe velar porque se diseñen, prueban y apliquen planes de contingencias relacionados con la misma.";
	echo "<br>";
	echo "<hr>";

	echo "<b>¿Es empleada la criptografía para proteger el almacenamiento y transmisión de información?</b>";
	echo "<br>";
	echo "La criptografía está exenta de regulados sistemas de almacenamiento centralizado de claves, adicionalmente es la protección indicada para aquellas personas que desean mantener la privacidad de la información que se almacena o se transmite a través de la red, porque es segura y confiable.";
	echo "<br>";
	echo "<hr>";
	
	echo "<b>¿Es tenido en cuenta el criterio de los usuarios de la aplicación durante las diferentes etapas del ciclo de vida del sistema?</b> ";
	echo "<br>";
	echo "La comunicación entre los usuarios y analista de sistemas debe ser amplia y cordial, y la opinión y experiencia del usuario debe ser tenida en cuenta tanto durante el análisis, como en el diseño, puesta en marcha y mantenimiento de la aplicación.";
	echo "<br>";
	echo "<hr>";


					}else  {
							echo "<hr>";
							echo "<b>Aspectos positivos:</b> ";
							echo "<br>";
							echo "<hr>";

							echo "Inhabilitan la clave de acceso de aquellos usuarios que entran a disfrutar de vacaciones o son retirados de la empresa.";
							echo "<br>";
							echo "<hr>";
					
							echo "Tiene los manuales técnicos, de operación y del usuario en físico y en línea.";
							echo "<br>";
							echo "<hr>";

							echo "Cuentan con un procedimiento que consigne todo lo relativo a las copias de seguridad. ";
							echo "<br>";
							echo "<hr>";

							echo "Tienen salvaguardas para que los programas de la aplicación no sean modificados, sustituidos o ejecutados por personas no autorizadas. ";
							echo "<br>";
							echo "<hr>";

							echo "Mantienen controlado el acceso a personas ajenas al sistema de computación e ingresar a la aplicación a través de menús obligatorios con solicitud de clave de acceso. ";
							echo "<br>";
							echo "<hr>";

							echo "Capacitan a todos sus funcionarios  ";
							echo "<br>";
							echo "<hr>";

							echo "Llevan un buen manejo de información de carácter confidencial. ";
							echo "<br>";
							echo "<hr>";

							echo "Implementan de controles que garantizan la protección de los datos que son transmitidos por la red. ";
							echo "<br>";
							echo "<hr>";

							echo "Revisan regularmente la bitácora de reinicios y reprocesos causados por el mal funcionamiento del sistema y analizan las causas con la finalidad de corregir tal dificultad. ";
							echo "<br>";
							echo "<hr>";

							echo "Cuentan con la documentación específica para el respaldo y recuperación de la base de datos. ";
							echo "<br>";
							echo "<hr>";

							echo "Cuentan con criptografía en su base de datos. ";
							echo "<br>";
							echo "<hr>";

							echo "Mantienen una buena comunicación entre los usuarios y analista de sistemas. ";
							echo "<br>";
							echo "<hr>";
					}
?>			
</div>
		</div>
		<br/>
		<center>
		<a href="index.php" class="btn btn-primary">Regresar</a>
			</center>
		</form>
 	</div>
	</center>


    
  	<!-- Optional JavaScript -->
  	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
  	<script src="../js/jquery-3.3.1.min.js"></script>
  	<script src="../js/popper.min.js"></script>
  	<script src="../js/bootstrap.min.js"></script>
</body>
</html>