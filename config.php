<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sistema_encuestasv";

// Creamos la conexión
$conexion = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conexion,"utf8");

// Verificamos la conexión
if ($conexion->connect_error) {
    die("Conexión fallida: " . $conexion->connect_error);
} else {
	// echo "Conexión exitosa";
}