<?php
	 
	require 'config.php';
	require 'funciones.php';
	
	if(empty($_GET['user_id'])){
		header('Location: index.php');
	}
	
	if(empty($_GET['token'])){
		header('Location: index.php');
	}
	
	$user_id = $conexion->real_escape_string($_GET['user_id']);
	$token = $conexion->real_escape_string($_GET['token']);
	
	if(!verificaTokenPass($user_id, $token))
	{
	echo 'No se pudo verificar los Datos';
	exit;
	}
	
	
?>

<html>
	<head>
		<title>Cambiar contraseña</title>
	<link rel="icon" type="image/png" href="https://cdn-icons-png.flaticon.com/512/3833/3833453.png">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet"  href="css/estilos.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>

		
	</head>
	
	<body>
	<section>
		<div class="container">    
			<div class="row">	
				<div class="col-sm-4"></div>
					<div  class="col-xs-12 col-sm-4">
						<div style="margin-top:0px" class="panel panel-default">
								<div class="panel-heading">
								<img src="img/cifrado.gif" height="100" width="100"/>
								<p><strong>Cambiar contraseña</strong></p>
								<div style="float:right; font-size: 100%; position: relative; top:-10px"><a href="index.php">Iniciar Sesión</a></div>
								</div> 
					
						<div class="panel-body">
						
						<form class="form-horizontal" role="form" action="guarda_pass.php" method="POST" autocomplete="off">
						<input type="hidden" id="user_id" name="user_id" value ="<?php echo $user_id; ?>" />
					
					      <input type="hidden" id="token" name="token" value ="<?php echo $token; ?>" />
								<div id="signupalert" style="display:none" class="alert alert-danger">
								<p>Error:</p>
								<span></span>
								</div>

							<div class="form-group">
							<label for="password" class="col-md-3 control-label">Nuevo contraseña</label>
							<div class="col-md-9">
							<input type="password" class="form-control" name="password" placeholder="Password" required>
							</div>
							</div>
					
							<div class="form-group">
							<label for="con_password" class="col-md-3 control-label">Confirmar contraseña</label>
							<div class="col-md-9">
							<input type="password" class="form-control" name="con_password" placeholder="Confirmar Password" required>
							</div>
							</div>
					
							<div style="margin-top:10px" class="form-group">
							<div class="col-sm-12 controls">
							<button id="btn-login" type="submit" class="btn btn-success">Modificar</a>
							</div>
							</div>   
						</form>         
					</div>
				</div>
					<div class="col-sm-4"></div>   
			</div>
	<br><br>
</section>
<footer style="margin-top: 450px;" id="footer2">
	<div class="container">
		<div class="row">
		</div>
	</div>	
</footer>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>