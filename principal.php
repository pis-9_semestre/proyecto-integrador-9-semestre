﻿﻿<?php
	session_start();

	include "config.php";
	
	if (isset($_SESSION['usuario']))
		{
	$id=$_SESSION['usuario'];
	$sql = "select * from tb_usuarios where usuario = '$id'";
	$resultado = $conexion->query( $sql );
	$row = mysqli_fetch_row($resultado);
?>
<!DOCTYPE html>  
<html lang="es">
<head>
<link rel="icon" type="image/png" href="https://cdn-icons-png.flaticon.com/512/3833/3833453.png">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="css/bootstrap.min.css">
	 <link href="css/jquery.dataTables.min.css" rel="stylesheet">
	 <link rel="stylesheet"  href="css/estilos.css">
	<script src="js/jquery-3.2.0.min.js" ></script>
	<script src="js/jquery.js" ></script>
	
	<title>Editar usuario</title>
</head>
<body> 
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-4"></div>
				<div  class="col-xs-12 col-sm-4">
					<div style="margin-top:-110px" class="panel panel-default">
					
						<div class="panel-heading">
							<img src="img/usuario.gif" height="100" width="100"/>
							<p><strong>Actualizar datos</strong></p>
						</div>

						
						<div class="col-xs-12"> 	
						<button type="button" style="float:left;" class="col-xs-12 btn btn-success" data-toggle="modal" data-target="#myModal2"><span class="glyphicon glyphicon-lock"></span> Cambiar contraseña </button>			
						</div>	

						<div class="panel-body">
							<div class=" container">
								<div class="row">
								
								<form class="form-horizontal" role="form" action="actualizar.php" method="POST" autocomplete="off">
									<div id="signupalert" style="display:none" class="alert alert-danger">
									<p>Error:</p>
									<span></span>
									</div>

									<div class="form-group">
									<label class="col-md-3">Usuario</label>
									<div class="col-md-12">
										<input type="text" class="form-control" name="usuario" placeholder="Usuario" value="<?php  echo $row[0];?>" required="">
									</div>
									</div>

									<div class="form-group">
									<label class="col-md-3">Nombre</label>
									<div class="col-md-12">
										<input type="text" class="form-control" name="nombre" placeholder="Nombre"onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122||event.charCode == 32))" value="<?php  echo $row[2];?>" required="">
									</div>
									</div>
								
									<div class="form-group">
									<label class="col-md-3">Celular</label>
									<div class="col-md-12">
										<input type="text" class="form-control" name="celular" placeholder="Celular" maxlength="10" onkeypress="return ((event.charCode >= 48 && event.charCode <= 57) )"  value="<?php  echo $row[3];?>" required="">
									</div>
									</div>
							
									<div class="form-group">
									<label class="col-md-3 ">Email</label>
									<div class="col-md-12">
										<input type="email" class="form-control" name="correo" placeholder="Email" value="<?php  echo $row[4];?>" required="">
									</div>
									</div>

									<div class="form-group">                                      
									<div class="col-xs-12">
									<button id="btn-signup" type="submit" class="col-xs-12 btn btn-success"><span class="glyphicon glyphicon-refresh"></span>  Actualizar Perfil</button> 
									</div>
									</div>

								</form>							
							</div>	
			 	 		</div>
					</div>
				<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						
						<h4 class="modal-title" id="myModalLabel"><i class="glyphicon glyphicon-edit"></i> Actualizar Contraseña</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					  </div>
					  <div class="modal-body">
						<form class="form-horizontal" method="post" action="actualizarpass.php" >
						 	<div class="form-group">
							<div class="col-sm-12">
							  <input type="password" class="form-control" id="documento" placeholder="Contraseña Actual" name="actualpass">
							</div>
						  	</div>
						  
							<div class="form-group">
							<div class="col-sm-12">
							  <input type="password" class="form-control" id="nombre" placeholder="Nueva Contraseña " name="nuevapass" required="">
							</div>
						 	</div>
						  
						  	<div class="form-group">
							<div class="col-sm-12">
							<input type="password" class="form-control" id="celular" placeholder="Confirma Nueva Contraseña" name="confirmapass">
							</div>
							</div>	
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary" id="actualizar_datos">Actualizar Contraseña</button>

					  </div>
					 	</form> 
						 
					</div>
				  </div>
				</div>		
				</div>
			<div class="col-sm-4"></div>
		</div>
		</div>
	</div>
</section>
<footer style="margin-top: 450px;" id="footer2">
	<div class="container">
		<div class="row">
		</div>
	</div>	
</footer>
		<?php
		 }else{
		 	header("location: index.php");
		 }
			include "inc/footer.php";
 		?>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>


